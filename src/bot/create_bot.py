
import os

from aiogram import Bot
from aiogram .dispatcher import Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from dotenv import load_dotenv

dotenv_path = os.path.join(os.getcwd(), 'config/.env')
load_dotenv(dotenv_path)

storage = MemoryStorage()

bot = Bot(token=os.getenv("TOKEN_BOT"))
dp = Dispatcher(bot, storage=storage)