
import asyncio

import aiogram
from aiogram import types, Dispatcher
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.state import State, StatesGroup

from src.bot.create_bot import bot
from src.bot.handlers.main_buttons import stop_menu, question_menu


class FSMChatGPT(StatesGroup):
    text = State()


async def question_start(message: types.Message) -> None:
    user_id: int = message.from_user.id
    text: str = "You can already ask your question"

    await bot.send_message(chat_id=user_id, text=text, reply_markup=stop_menu)
    await FSMChatGPT.text.set()


async def question(message: types.Message, state: FSMChatGPT) -> None:
    user_id: int = message.from_user.id
    question_text: str = message.text
    finish_text: str = "Thank you"

    if question_text != "stop":
        await bot.send_message(chat_id=user_id, text=question_text, reply_markup=stop_menu)

    else:
        await state.finish()
        await bot.send_message(chat_id=user_id, text=finish_text, reply_markup=question_menu)


def register_messages(dp: Dispatcher):
    dp.register_message_handler(question_start, Text(equals='question', ignore_case=True))
    dp.register_message_handler(question, state=FSMChatGPT.text)



