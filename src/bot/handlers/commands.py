
import asyncio

import aiogram
from aiogram import types, Dispatcher
from src.bot.create_bot import bot
from src.database.crud import create_user, create_message
from src.bot.handlers.main_buttons import question_menu


async def start_cm(message: types.Message) -> None:
    user_data: dict = message.from_user if message.from_user else {}
    user_id: int = message.from_user.id if message.from_user.id else None
    text: str = ""

    try:
        user = create_user(user_data)
        if user["status"] == "such user already exists":
            text = "for more information press the /help command"
        elif user["status"] == "success":
            text = "Welcome\nfor more information press the /help command"
        else:
            text = "A server error has occurred"

    except Exception as ex:
        text = "Error occurred while getting user data: " + str(ex)

    await bot.send_message(chat_id=user_id, text=text, reply_markup=question_menu)


async def help_cm(message: types.Message) -> None:
    user_id: int = message.from_user.id if message.from_user.id else None
    text: str = "Welcome, This is bot for company HUMMUS."

    await bot.send_message(chat_id=user_id, text=text, reply_markup=question_menu)


def register_commands(dp: Dispatcher) -> None:
    dp.register_message_handler(start_cm, commands=["start"])
    dp.register_message_handler(help_cm, commands=["help"])
