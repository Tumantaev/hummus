
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

question_btn = KeyboardButton('question')
stop_btn = KeyboardButton('stop')

question_menu = ReplyKeyboardMarkup(resize_keyboard=True)
question_menu.add(question_btn)

stop_menu = ReplyKeyboardMarkup(resize_keyboard=True)
stop_menu.add(stop_btn)
