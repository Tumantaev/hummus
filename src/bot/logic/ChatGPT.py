
import os

from llama_index import GPTVectorStoreIndex, Document, SimpleDirectoryReader
from dotenv import load_dotenv

dotenv_path = os.path.join(os.getcwd(), '.env')
load_dotenv(dotenv_path)

os.environ['OPENAI_API_KEY'] = os.getenv("OPENAI_API_KEY")