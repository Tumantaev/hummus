
import os

from create_bot import dp
from aiogram.utils import executor
from src.bot.handlers.commands import register_commands
from src.bot.handlers.messages import register_messages
from src.database.create_table import start_db


def main():

    start_db().create_table()

    register_commands(dp)
    register_messages(dp)
    executor.start_polling(dp, skip_updates=True)


if __name__ == '__main__':
    main()