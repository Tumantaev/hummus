
import os
import psycopg2

from dotenv import load_dotenv

dotenv_path = os.path.join(os.getcwd(), '.env')
load_dotenv(dotenv_path)


class DatabaseManager:

    def __init__(self, host: str, user: str, password: str, db_name: str):
        self.__host: str = host
        self.__user: str = user
        self.__password: str = password
        self.__db_name: str = db_name
        self.connection = None

    def connect(self):
        try:
            self.connection = psycopg2.connect(
                host=self.__host,
                user=self.__user,
                password=self.__password,
                database=self.__db_name
            )
            self.connection.autocommit = True

        except psycopg2.Error as e:
            print("Error:", e)

        return self.connection

    def create_table(self):
        if self.connection is None:
            self.connect()

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("""
                    CREATE TABLE IF NOT EXISTS users (
                        increment_id SERIAL PRIMARY KEY,
                        telegram_user_id bigint NOT NULL,
                        username varchar(50) NOT NULL,
                        registration_date DATE NOT NULL DEFAULT CURRENT_DATE,
                        created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
                    )
                """)

                cursor.execute("""
                    CREATE TABLE IF NOT EXISTS message_user (
                        id SERIAL PRIMARY KEY,
                        user_id bigint NOT NULL,
                        message text NOT NULL,
                        mesaage_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        FOREIGN KEY (user_id) REFERENCES users (increment_id)
                    )
                """)

                cursor.execute("""
                        CREATE TABLE IF NOT EXISTS message_openai (
                            id SERIAL PRIMARY KEY,
                            user_id bigint NOT NULL,
                            message text NOT NULL,
                            mesaage_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            FOREIGN KEY (user_id) REFERENCES users (increment_id)
                        )
                    """)

        except psycopg2.Error as e:
            print("Error:", e)

    def create_user(self, user: dict) -> dict:
        if self.connection is None:
            self.connect()

        telegram_user_id: int = user["id"]
        username: str = user["username"]
        result: dict = {}

        with self.connection.cursor() as cursor:
            try:
                cursor.execute("""
                        ALTER TABLE users ADD CONSTRAINT unique_telegram_user_id UNIQUE (telegram_user_id);
                    """)

                cursor.execute("""
                        INSERT INTO users (telegram_user_id, username, registration_date, created_at, updated_at)
                        VALUES (%s, %s, CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
                        ON CONFLICT (telegram_user_id) DO NOTHING
                        RETURNING increment_id
                    """, (telegram_user_id, username))

                result["status"]: str = "success"

            except Exception as ex:
                result["status"]: str = "error"

        return result

    def create_message_user(self, data: dict) -> dict:
        if self.connection is None:
            self.connect()

        user_id: int = data["user_id"]
        message: str = data["text"]
        result: dict = {}

        with self.connection.cursor() as cursor:
            try:
                cursor.execute("""
                    INSERT INTO message_user ( user_id, message, mesaage_date)
                    VALUES ( %s, %s, CURRENT_DATE)
                """, (user_id, message))

                result["status"]: str = "success"

            except Exception as ex:
                result["status"]: str = "error"

        return result

    def create_message_openai(self, data: dict) -> dict:
        if self.connection is None:
            self.connect()

        user_id: int = data["user_id"]
        message: str = data["text"]
        result: dict = {}

        with self.connection.cursor() as cursor:
            try:
                cursor.execute("""
                    INSERT INTO message_openai ( user_id, message, mesaage_date)
                    VALUES ( %s, %s, CURRENT_DATE)
                """, (user_id, message))

                result["status"]: str = "success"

            except Exception as ex:
                result["status"]: str = "error"

        return result

    def drop_table(self, db_name):
        if self.connection is None:
            self.connect()

        try:
            with self.connection.cursor() as cursor:
                cursor.execute("DROP TABLE IF EXISTS {db_name}".format(db_name=db_name))
        except psycopg2.Error as e:
            print("Error:", e)

    def close_connection(self):
        if self.connection is not None:
            self.connection.close()


def start_db():

    db_manager = DatabaseManager(
        host=os.getenv('DB_HOST'),
        user=os.getenv('DB_USER'),
        password=os.getenv('DB_PASSWORD'),
        db_name=os.getenv('DB_NAME')
    )

    return db_manager
